<?php

class Checkout_fi {
	private $version="0001";
	private $language="FI";
	private $country="FIN";
	private $currency="EUR";
	private $device="1";
	private $content="1";
	private $type="0";
	private $algorithm="1";
	private $merchant="";
	private $password="";
	private $stamp=0;
	private $amount=0;
	private $reference="";
	private $message="";
	private $return="";
	private $cancel="";
	private $reject="";
	private $delayed="";
	private $delivery_date="";
	private $firstname="";
	private $familyname="";
	private $address="";
	private $postcode="";
	private $postoffice="";
	private $status="";

	public function __construct($merchant,$password) {
		$this->merchant=$merchant;
		$this->password=$password;
	}

	public function generateCheckout($data) {
		foreach($data as $key => $value) {
			$this->{$key}=$value;
		}
		$mac=strtoupper(md5("{$this->version}+{$this->stamp}+{$this->amount}+{$this->reference}+{$this->message}+{$this->language}+{$this->merchant}+{$this->return}+{$this->cancel}+{$this->reject}+{$this->delayed}+{$this->country}+{$this->currency}+{$this->device}+{$this->content}+{$this->type}+{$this->algorithm}+{$this->delivery_date}+{$this->firstname}+{$this->familyname}+{$this->address}+{$this->postcode}+{$this->postoffice}+{$this->password}"));
		$post['VERSION']=$this->version;
		$post['STAMP']=$this->stamp;
		$post['AMOUNT']=$this->amount;
		$post['REFERENCE']=$this->reference;
		$post['MESSAGE']=$this->message;
		$post['LANGUAGE']=$this->language;
		$post['MERCHANT']=$this->merchant;
		$post['RETURN']=$this->return;
		$post['CANCEL']=$this->cancel;
		$post['REJECT']=$this->reject;
		$post['DELAYED']=$this->delayed;
		$post['COUNTRY']=$this->country;
		$post['CURRENCY']=$this->currency;
		$post['DEVICE']=$this->device;
		$post['CONTENT']=$this->content;
		$post['TYPE']=$this->type;
		$post['ALGORITHM']=$this->algorithm;
		$post['DELIVERY_DATE']=$this->delivery_date;
		$post['FIRSTNAME']=$this->firstname;
		$post['FAMILYNAME']=$this->familyname;
		$post['ADDRESS']=$this->address;
		$post['POSTCODE']=$this->postcode;
		$post['POSTOFFICE']=$this->postoffice;
		$post['MAC']=$mac;
		return $post;
	}

	public function getCheckoutXML($data) {
		$this->device="10";
		return $this->sendPost($this->generateCheckout($data));
	}

	private function sendPost($post) {
		$data="";
		foreach($post as $key => $value) {
		  	$data.=urlencode($key)."=".urlencode($value)."&";
		}

		$fp = fsockopen("ssl://payment.checkout.fi", 443);
		fputs($fp, "POST / HTTP/1.1\r\n");
		fputs($fp, "Host: payment.checkout.fi\r\n");
		fputs($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "User-Agent: Checkout-poster/1.0\r\n");
		fputs($fp, "Content-Length: " . strlen($data) . "\r\n");
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);

		while (!feof($fp)) $sent .= fgets($fp,128);
		fclose($fp);
		$tmp=split("\r\n\r\n",$sent,2);
		$return=split("\n",$tmp[1],2);
		return substr($return[1],0,-8);
	}
	
	public function toStamp($viite) {
		$summa = 0;
		$viite = trim($viite);
		$taulukko = preg_split('//', $viite, -1, PREG_SPLIT_NO_EMPTY);
		$painoarvot = array(7, 3, 1);
		$indeksi = 0;
		for ($i = ((strlen($viite)) - 1); $i >=0; $i--)	{
			$summa = $summa + ($painoarvot[$indeksi] * $taulukko[$i]);
			$indeksi++;
			if ($indeksi > 2) $indeksi = 0;
		}
		$summa = (10 - ($summa % 10)) % 10;
		$viite = $viite . $summa;
		return $viite;
	}
	public function validateCheckout($data) {
		$mac="";
		foreach($data as $key => $value) {
			$key=strtolower($key);
			if($key!="mac") $this->{$key}=$value;
			else $mac=$value;
		}
		$generatedMac=strtoupper(md5("{$this->version}+{$this->stamp}+{$this->reference}+{$this->payment}+{$this->status}+{$this->algorithm}+{$this->password}"));
		if($mac==$generatedMac) {
			return true;
		}
		return false;
	}

	public function isPaid() {
		if($this->status!="") {
			if(in_array($this->status,array(2,4,5,6,7,8,9,10)))
				return true;
		}
		return false;
	}
}