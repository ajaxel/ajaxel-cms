<?php
class Checkout {
	private $version="0001";
	private $language="FI";
	private $country="FIN";
	private $currency="EUR";
	private $device="1";
	private $content="1";
	private $type="0";
	private $algorithm="1";
	private $merchant="";
	private $password="";
	private $stamp=0;
	private $amount=0;
	private $reference="";
	private $message="";
	private $return="";
	private $cancel="";
	private $reject="";
	private $delayed="";
	private $delivery_date="";
	private $firstname="";
	private $familyname="";
	private $address="";
	private $postcode="";
	private $postoffice="";
	private $status="";
	
	public function __construct($merchant,$password) {
		$this->merchant=$merchant;
		$this->password=$password;
	}
		
	public function generateCheckout($data) {
		foreach($data as $key => $value) {
			$this->{$key}=$value;
		}
		$mac=strtoupper(md5("{$this->version}+{$this->stamp}+{$this->amount}+{$this->reference}+{$this->message}+{$this->language}+{$this->merchant}+{$this->return}+{$this->cancel}+{$this->reject}+{$this->delayed}+{$this->country}+{$this->currency}+{$this->device}+{$this->content}+{$this->type}+{$this->algorithm}+{$this->delivery_date}+{$this->firstname}+{$this->familyname}+{$this->address}+{$this->postcode}+{$this->postoffice}+{$this->password}"));
		$post['VERSION']=$this->version;
		$post['STAMP']=$this->stamp;
		$post['AMOUNT']=$this->amount;
		$post['REFERENCE']=$this->reference;
		$post['MESSAGE']=$this->message;
		$post['LANGUAGE']=$this->language;
		$post['MERCHANT']=$this->merchant;
		$post['RETURN']=$this->return;
		$post['CANCEL']=$this->cancel;
		$post['REJECT']=$this->reject;
		$post['DELAYED']=$this->delayed;
		$post['COUNTRY']=$this->country;
		$post['CURRENCY']=$this->currency;
		$post['DEVICE']=$this->device;
		$post['CONTENT']=$this->content;
		$post['TYPE']=$this->type;
		$post['ALGORITHM']=$this->algorithm;
		$post['DELIVERY_DATE']=$this->delivery_date;
		$post['FIRSTNAME']=$this->firstname;
		$post['FAMILYNAME']=$this->familyname;
		$post['ADDRESS']=$this->address;
		$post['POSTCODE']=$this->postcode;
		$post['POSTOFFICE']=$this->postoffice;
		$post['MAC']=$mac;
		return $post;
	}
	
	public function getCheckoutXML($data) {
		$this->device="10";
		return $this->sendPost($this->generateCheckout($data));
	}
	
	private function sendPost($post) {
		$data="";
		foreach($post as $key => $value) {
		  	$data.=urlencode($key)."=".urlencode($value)."&";
		}
		
		$fp = fsockopen("ssl://payment.checkout.fi", 443);
		fputs($fp, "POST / HTTP/1.1\r\n");
		fputs($fp, "Host: payment.checkout.fi\r\n");
		fputs($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
		fputs($fp, "User-Agent: Checkout-poster/1.0\r\n");
		fputs($fp, "Content-Length: " . strlen($data) . "\r\n");
		fputs($fp, "Connection: close\r\n\r\n");
		fputs($fp, $data);
	
		while (!feof($fp)) $sent .= fgets($fp,128);
		fclose($fp);
		$tmp=split("\r\n\r\n",$sent,2);
		$return=split("\n",$tmp[1],2);
		return substr($return[1],0,-8);
	}
	
	public function validateCheckout($data) {
		$mac="";
		foreach($data as $key => $value) {
			$key=strtolower($key);
			if($key!="mac") $this->{$key}=$value;
			else $mac=$value;
		}
		$generatedMac=strtoupper(md5("{$this->version}+{$this->stamp}+{$this->reference}+{$this->payment}+{$this->status}+{$this->algorithm}+{$this->password}"));
		if($mac==$generatedMac) {
			return true;
		} 
		return false;
	}
	
	public function isPaid() {
		if($this->status!="") {
			if(in_array($this->status,array(2,4,5,6,7,8,9,10))) 
				return true;
		}
		return false;
	}
} 
$co=new Checkout(375917,"SAIPPUAKAUPPIAS");

if(isset($_GET['MAC'])) {
	if($co->validateCheckout($_GET)) {
		echo("Checkout transaction MAC CHECK OK.");
		if($co->isPaid()) {
			echo("Paid.");
		} else {
			echo("Not paid.");
		}
	} else {
		echo("Checkout transaction MAC CHECK Failed.");	
	}
	exit;
}

// Normal mode w/ checkout button only
$coData=array();
$coData["stamp"]=time();
$coData["reference"]="12344";
$coData["message"]="Huonekalutilaus\nPaljon puita ja muttereita";
$coData["return"]="http://demo1.checkout.fi/xml2.php?test=1";
$coData["amount"]="1000";
$coData["delivery_date"]=date("Ymd");
$coData["firstname"]="Tero";
$coData["familyname"]="Testaaja";
$coData["address"]="��kk�stie 5b3\nKulmaravintolan yl�kerta";
$coData["postcode"]="33100";
$coData["postoffice"]="Tampere";
$post=$co->generateCheckout($coData);


// XML mode with bank buttons
$coData['stamp']=time()+1;
$return=$co->getCheckoutXML($coData);
$xml=simplexml_load_string($return);


// Render the buttons
echo("<html><head><title>Checkout maksudemo</title></head><body><h1>Normaali maksutapa</h1>\n");
echo("<form action='https://payment.checkout.fi/' method='post'>\n");
foreach($post as $field => $value) {
	echo("<input type='hidden' name='$field' value='$value' />\n");
}
echo("<input type='submit' value='maksuun' /></form>\n");

echo("<hr /><h1>XML maksunapit</h1>\n");
foreach($xml->payments->payment->banks as $bankX) {
	foreach($bankX as $bank) {
		echo("<div style='float: left; margin-right: 20px; min-height: 100px;'><form action='{$bank['url']}' method='post'>\n");
		foreach($bank as $key => $value) {
			echo("<input type='hidden' name='$key' value='$value' />\n");
		}
		echo("<input type='image' src='{$bank['icon']}' /></form></div>\n");
	}
}
echo("</body></html>");

