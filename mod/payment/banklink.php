<?
/**
* More info at http://www.kaubandus.fantaasia.com/votmed.html
*/
final class BankLink {
	
	var $charset = 'utf-8';
	var $charset_parameter = '';
	
	var $VK_variableOrder = array(
		1001 => array(
            'VK_SERVICE','VK_VERSION','VK_SND_ID',
            'VK_STAMP','VK_AMOUNT','VK_CURR',
            'VK_ACC','VK_NAME','VK_REF','VK_MSG'
        ),

        1101 => array(
            'VK_SERVICE','VK_VERSION','VK_SND_ID',
            'VK_REC_ID','VK_STAMP','VK_T_NO','VK_AMOUNT','VK_CURR',
            'VK_REC_ACC','VK_REC_NAME','VK_SND_ACC','VK_SND_NAME',
            'VK_REF','VK_MSG','VK_T_DATE'
        ),

        1901 => array(
            'VK_SERVICE','VK_VERSION','VK_SND_ID',
            'VK_REC_ID','VK_STAMP','VK_REF','VK_MSG'
        ),
	);
	
	function BankLink() {
		switch (BANK) {
			case 'swedbank':
				$this->charset_parameter = '';
			break;
			case 'seb':
				$this->charset_parameter = 'VK_CHARSET';
			break;
			case 'sampo':
				$this->charset = 'iso-8859-1';
			break;
			case 'nordea':
				
			break;			
		}
		
		

	}


	/**\
	* Paste here your key's password
	*/
	function _pass() {
		$pass = '';
		switch (BANK) {
			case 'swedbank':
				$pass = '';
			break;
			case 'seb':
				$pass = '';
			break;
			case 'sampo':
				$pass = '';
			break;
			case 'nordea':
				$pass = '';
			break;			
		}
		return $pass;
	}

	/**\
	* Paste here your server generated key
	*/	
	function _privKey() {
		$file = false;
		switch (BANK) {
			case 'swedbank':
				$file = FTP_DIR_TPL.'classes/pem/swedbank_private_key.pem';
			break;
			case 'seb':
				$file = FTP_DIR_TPL.'classes/pem/seb_private_key.pem';
			break;
			case 'sampo':
				$file = FTP_DIR_TPL.'classes/pem/sampo_private_key.pem';
			break;
			case 'nordea':
				$file = '';
			break;			
		}
		if (!$file) return '';
		return file_get_contents($file);
	}

	/**\
	* write here file path to the file which bank gave you
	*/
	function keyFile() {
		switch (BANK) {
			case 'swedbank':
				$file = FTP_DIR_TPL.'classes/pem/swedbank.pem';
			break;
			case 'seb':
				$file = FTP_DIR_TPL.'classes/pem/eypcert.pem';
			break;
			case 'sampo':
				$file = FTP_DIR_TPL.'classes/pem/sampocert.pem';
			break;
			case 'nordea':
				$file = FTP_DIR_TPL.'classes/pem/nordeacert.pem';
			break;			
		}
		return $file;
	}
	
	
	
	/**\
	* Do not touch below
	*/
	function createSignature($VK_a) {
		if (!function_exists('openssl_get_privatekey')) {
			die('openssl_get_privatekey is not installed!');
		}
		$data = '';
		if (!isset($this->VK_variableOrder[intval($VK_a['VK_SERVICE'])])) {
			p($VK_a);
			echo 'Does not exists: '.$VK_a['VK_SERVICE'];
			exit;
		}
		foreach ($this->VK_variableOrder[intval($VK_a['VK_SERVICE'])] as $key) {
			$data .= $this->_padIt($VK_a[$key]);
		}
		/*
		if ($this->charset_parameter && isset($VK_a[$this->charset_parameter])) {
			$data .= $this->_padIt($VK_a[$this->charset_parameter]);
		}
		*/
		$res = openssl_get_privatekey($this->_privKey(),$this->_pass());
		if (!$res) {
			die('ERROR: openssl_get_privatekey() returns false, wrong key/password');
		}
		openssl_sign($data, $signature, $res);
		$VK_MAC = base64_encode($signature);
		openssl_free_key($res);
		return $VK_MAC;
	}
	function unhtmlentities($string) {
		// replace numeric entities
		$string = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $string);
		$string = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $string);
		// replace literal entities
		$trans_tbl = get_html_translation_table(HTML_ENTITIES);
		$trans_tbl = array_flip($trans_tbl);
		return strtr($string, $trans_tbl);
	}
	function verifySignature($VK_a) {
		if (!isset($VK_a['VK_SERVICE'])) return false;
		$signature = base64_decode($VK_a['VK_MAC']);
		$data = '';
		foreach ($this->VK_variableOrder[$VK_a['VK_SERVICE']] as $key) {
			$val = $VK_a[$key];
			$data .= $this->_padIt($val);
		}
		$cert = $this->_readPubKey();
		$pubkey = openssl_get_publickey($cert);
		$out = openssl_verify($data, $signature, $pubkey);
		openssl_free_key($pubkey);
		return $out;
	}
	function _readPubKey()	{
		$key_file = $this->keyFile();
		$fp = fopen($key_file, "r");
		$cert = fread($fp, 8192);
		fclose($fp);
		return $cert;
	}
	function _padIt($str) {
		$l = ((BANK=='swedbank') ? mb_strlen ($str, $this->charset) : strlen($str)); 
		return str_pad($l,3,'0',STR_PAD_LEFT).$str;
	}
}