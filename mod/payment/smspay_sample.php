<?php include('smspay.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>SMS-pay sample app</title>
    <link href="http://smspay.fortumo.com/stylesheets/smspay.css" media="screen" rel="Stylesheet" type="text/css" />
  </head>
  <body>
    <div style="width: 400px;">
      <?php echo $smspay_resp; ?>
    </div>
  </body>
</html>
<?php

/* library with necessary functions: */
include('for_smspay.php');

$my_pay = new ForSmspay(
        '0229583d6707c5ece64d3caf46071985', // your SMS-pay service id
        '18e77a09843a01d32aa8a0027493129f' // your SMS-pay service secret
);

/*
* NB! Response headers are set by session_start() function and thus,
* it must be executed before generating the response body.
* 
* If your application already calls session_start() before executing this script,
* it will be recommended to remove the function call:

if ($my_pay->sessions) {
	session_start();
}
*/
/* The content can be displayed by another script which generates HTML output: */
global $smspay_resp;

if ($my_pay->payment_verified()) {
	/* User entered a valid password, authentication successful and the content can be displayed to the user */
	$smspay_resp = $my_pay->restricted_content();

  /* Or you can redirect the user to a separate script which verifies that $_SESSION["payment_ok"] is set: */
  /* 
   * header('HTTP/1.0 302 Moved temporarily');
   * header('Location: content.php');
   */
} else {
	/*
	 * Invalid password or an error occurred;
	 * display the login widget with an error message
	 */
	$smspay_resp = $my_pay->widget();
}

?>