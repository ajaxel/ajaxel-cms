<?php

/**
* Ajaxel CMS v8.0
* http://ajaxel.com
* =================
* 
* Copyright (c) 2007-2016, Alexander Shatalov <ajaxel.com@gmail.com>. All rights reserved.
* 
* The software, this file and its contents are subject to the Ajaxel CMS
* License. Please read the license.txt file before using, installing, copying,
* modifying or distribute this file or part of its contents. The contents of
* this file is part of the source code of Ajaxel CMS.
* 
* @file       tpls/admin/visual_window.php
* @category   Content management system
* @package    Ajaxel CMS
* @version    8.0, 15:25 2015-12-23
* @copyright  Copyright (c) 2007-2016, Alexander Shatalov <ajaxel.com@gmail.com>. All rights reserved.
* @license    http://ajaxel.com/license.txt
*/

?><script type="text/javascript">
var window_<?php echo $this->name_id?> = {
	load:function() {
		<?php echo $this->inc('js_load', array('win'=>true))?>
		VisualEditor.load();
	}
	,maximize:function(win_id){
		var o=S.A.W.wins[win_id];
		if (o.maximized) {
			o.win.children(0).animate({
				width:o.w2,
				height: (o.h2?o.h2:null)
			}, function(){
				o.maximized=false;	
			}).find('.a-window-title').css({
				width:o.w2-150
			});
			o.win.animate({
				width:o.w2,
				height: (o.h2?o.h2:null),
				top:o.t2,
				left: o.l2
			}, function(){
				o.maximized=false;	
			});
			$('#a-visual_body').animate({
				height: 500
			});
			S.A.W.minimized(win_id);
		} else {
			o.h2=parseInt(o.win.children(0).height());
			var w=$(window).width()-50;
			var h=$(window).height()-40;
			o.win.children(0).animate({
				width:w,
				height:h
			}, function(){
				o.maximized=true;
				$(this).find('.a-window-title').css({
					width:w-150
				});
			});
			o.win.animate({
				width:w,
				height:h,
				top:10,
				left:25
			}, function(){
				o.maximized=true;	
			});
			$('#a-visual_body').animate({
				height: h-70
			});
			S.A.W.maximized(win_id);
		}
	}
}
<?php $this->inc('js_pop')?>
var VisualEditor ={
	load:function(){
		
	},
	save:function(close){
		S.A.L.json('?<?php echo URL_KEY_ADMIN?>=global', {
			get: 'action',
			a: 'visual_save',
			<?php if ($this->post('visual_add', false)):?>
			visual_add: '<?php echo $this->post('visual_add')?>',
			above: <?php echo $this->post('above')?>,
			tag: $('.a-radio_<?php echo $this->name_id?>:checked').val(),
			<?php else:?>
			visual_id: '<?php echo $this->post('visual_id')?>',
			<?php endif;?>
			html: $('#a-visual_body').val()
		},function(data){
			if (close) S.A.W.close();
			S.G.msg(data);
			if (data.files) S.A.V.put_files(data.files);
			if (data.visual_add) {
				var span = $('<span>').html(data.html);
				if (data.above==1) {
					$('#'+data.id).before(span);
				} else {
					$('#'+data.id).after(span);
				}
			}
			else if (data.html) {
				$('#'+data.id).html(data.html);
			}
		});
	}
	,image:function(){
		S.A.W.open('?<?php echo URL_KEY_ADMIN?>=global&a=image_browser');
	}
	,keyUp:function(e){
		if (!e||!e.keyCode) e=window.event;
		if ((e.keyCode==83&&(e.ctrlKey||e.altKey))) {
			this.save(true);
		}
	}
}
</script>
<?php 
$this->title = $this->post('title', false);
$this->width = 900;
$this->inc('window_top');

?><form method="post" class="window_form" id="a-form_<?php echo $this->name_id?>">
<?php
if ($this->post('visual_add', false)):?>
<table cellspacing="0" cellpadding="0"><tr>
<td class="a-td2">
	<?php echo lang('Start tag').': ';
		$tags = explode('|',trim(VISUAL_TAGS,'()'));
		echo '&nbsp;&nbsp; <label for="a-tag_0" style="border-bottom:none"><input type="radio" class="a-radio_'.$this->name_id.'" id="a-tag_0" value=""> none</label>';
	foreach ($tags as $i => $t) {
		$i++;
		echo '&nbsp; <label for="a-tag_'.$i.'" style="border-bottom:none"><input type="radio" class="a-radio_'.$this->name_id.'" value="'.$t.'" id="a-tag_'.$i.'"'.($t=='div'?' checked="checked"':'').'> '.strtoupper($t).'</label>';
	}
	?>
</td>
</tr></table>
<?php
endif;
?>
<textarea id="a-visual_body" style="font:12px monospace;height:500px;width:99%;" onkeydown="VisualEditor.keyUp(event)"><?php echo strform($this->post('html', false))?></textarea>
<div class="a-window-bottom">
<table cellspacing="0" cellpadding="0" style="width:100%"><tr>
<td class="a-td1" style="text-align:left">
	<?php $this->inc('button',array('class'=>'a-button a-button_s','click'=>'VisualEditor.image()','text'=>lang('$Insert an image'))); ?>
</td>
<td class="a-td2">
	<?php $this->inc('button',array('class'=>'a-button a-button_b','click'=>'VisualEditor.save(true)','img'=>'oxygen/16x16/actions/document-save.png','text'=>lang('$Save'))); ?>
</td>
</tr></table>
</div>
</form>
<?php $this->inc('window_bottom')?>