<?php

final class MyClass extends My {
	public function __construct(&$Index) {
		parent::__construct($Index);
		$this->version = '1.00';
	}
	
	public function init() {
		
	}
	
	public function head() {
		if (!IS_ADMIN) $this->Index->addCSSA('ui/dark-hive/'.JQUERY_CSS);
		parent::head();
	}
	
	public function prefix() {
		$this->set('site_design', '');

		switch (url(0)) {
			case '':
			case URL_KEY_HOME:
				$this->set('site_design', 'main');
				return '';
			break;
			default:
				return '';
			break;
		}
	}	
	



	public function json() {
		switch (url(0)) {
			case 'something':
			
			break;
			default:
				parent::json(true);
			break;
		}
		$this->end();
	}
	
	/**
	* Called right before index template file
	*/
	final public function index() {
		$this->arrMenu = Factory::call('menu')->getByPosition(array('top','bottom'));	
		$this->set('arrMenu', $this->arrMenu);
		$this->set('url0', url(0,false,true));
		$this->set('url1', url(1,false,true));
	}
	
	
	
	public function data($type, $toOptions = -1, $optSettings = 'dropdown') {
		list ($type, $key, $keyAsVal, $lang) = Html::startArray($type);
		switch ($type) {
			case 'year_range':
				$ret = Html::arrRange(2010, 2040);
			break;
		}
		return Html::endArray($ret, $keyAsVal, $lang, $key, $toOptions, $optSettings);		
	}


	public function uploadify() {
		Factory::call('uploadify',$this->Index)->name('profile')->id($this->UserID)->folder('users', true)->sizes(array(
			1	=> array(800, 600)
			,2	=> array(148, 170)
			,3	=> array(50, 50)
		))->th(2)->tpl('includes/pic.tpl')->ext(array('jpeg','jpg','gif','png'))->desc('Image files')->one('photo')->end();
		
		Factory::call('uploadify',$this->Index)->name('grid_swap')->folder('grid_swap', false)->sizes(array(
			1	=> array(800, 600)
			,2	=> array(240, 240)
			,3	=> array(50, 50)
		))->th(2)->tpl('includes/pic.tpl')->ext(array('jpeg','jpg','gif','png'))->desc('Image files')->one('photo')->end();

	}	
	
	
	public function uploaded($name, $f, $id) {
		if ($name=='profile') {
			if ($id!=$this->UserID) {
				throw new Exception('ID is not userid');
			}
			$this->Index->Session->Photo = $f['file'];
			DB::run('UPDATE '.DB_PREFIX.'users SET main_photo='.escape($f['file']).' WHERE id='.$this->UserID);
		}
		elseif ($name=='grid_swap') {
			$_SESSION['grid_swap_photo'] = $f['file'];
		}
	}
	
	public function deleted($name, $file, $id) {
		if ($name=='profile') {
			if ($id!=$this->UserID) {
				throw new Exception('ID is not userid');
			}
			$this->Index->Session->Photo = '';
			DB::run('UPDATE '.DB_PREFIX.'users SET main_photo=\'\' WHERE id='.$this->UserID);
		}
		elseif ($name=='grid_swap') {
			$_SESSION['grid_swap_photo'] = '';
		}
	}

	public function upload() {
		parent::upload();	
	}
	
	protected function rated($data) {

		// data = id, [rate_old], rate, table, id
		if (!$data['id']) return;
		$rate = DB::one('SELECT SUM(rate)/COUNT(1) FROM '.DB_PREFIX.'votes WHERE setid='.$data['id'].' AND `table`='.escape($data['table']).'');
		DB::run('UPDATE '.$this->prefix.'grid_djs SET rate_logo='.(float)$rate.' WHERE id='.$data['id']);
	}

	
	public function page() {
		$data = array();

		switch (url(0)) {
			case 'user':
				if (!Factory::call('user')->getContent()) {
					$this->Index->mainarea = false;
				}
			break;
			case 'tag':
				$this->Index->setVar('page_title',lang('Tag results'));
				$this->Index->setVar('title',lang('Tag results'));
				if (!Factory::call('content')->setLimit(10)->getTag(get('tag'))) {
					$this->Index->mainarea = false;
				}
			break;
			case 'search':
				$this->Index->setVar('page_title',lang('Search results'));
				$this->Index->setVar('title',lang('Search results'));
				if (!Factory::call('content')->setLimit(10)->getSearch(get('search'))) {
					$this->Index->mainarea = false;
				}
			break;
			case 'sitemap':
				Factory::call('content')->getSitemap();
				$this->Index->tree[0] = array(
					'title'	=> lang('Sitemap'),
					'url'	=> '?sitemap'
				);
				$this->Index->setVar('title', lang('Sitemap'));
			break;
			case 'contact':
				$this->contact();
			break;
			case URL_KEY_HOME:
				$this->Index->displayFile('main.tpl');				
			break;
			default:
				if (!$this->Index->staticPage(url(0),url(1)) && 
					!Factory::call('content')->setLimit(20)->setOrder('dated DESC, sort, id DESC')->setFilter('')->getContent()) {
					$this->Index->mainarea = false;
				}
			break;
		}		
	}
		
}