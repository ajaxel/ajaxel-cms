<!DOCTYPE html><html><head><title><?=$this->getVar('title')?></title>
<meta name="keywords" content="<?=$this->getVar('keywords')?>" />
<meta name="description" content="<?=$this->getVar('description')?>" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="<?=$this->getVar('charset')?>" />
<link rel="alternate" type="application/rss+xml" href="<?=HTTP_BASE?>rss.xml" />
<link rel="shortcut icon" type="image/x-icon" href="<?=$this->getVar('favicon')?>" />
<?php echo $this->getVar('conf').$this->getVar('css').$this->getVar('js')?>
</head>