<?php

/**
* Database configuration
* for Ajaxel CMS v6.4
* Author: Alexander Shatalov <admin@ajaxel.com>
* http://ajaxel.com
*/

/*
switch ($_SERVER['HTTP_HOST']) {
	case 'cms':
	default:
		define ('DB_HOST','127.0.0.1'); // or localhost
		define ('DB_USERNAME','root');
		define ('DB_PASSWORD','');
		define ('DB_NAME','cms');
		define ('DB_PERSISTENT', false);
		define ('DB_PREFIX','');
		define ('HTTP_BASE','http://cms/');
		define ('DEFAULT_TEMPLATE', 'social');
		define ('LICENSE_KEY','');
	break;
}
*/

define ('HTTP_BASE', 'http://cms/');

$_conf['DB'] = array(
	'cms'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'cms',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'cms',
	),
	'social'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'social',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'social',
	),
	'room'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'room',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'room',
	),
	'balt'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'balt',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'balt',
	),
	'expo'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'expo',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'expo',
	),
	'autokool'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'autokool',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'autokool',
	),
	'homes'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'homes',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'proinvest',
		'define'	=> array(
			'FACEBOOK_APP_ID'		=> '',
			'FACEBOOK_APP_SECRET'	=> ''
		)
	),
	'default'	=> array(
		'HOST'		=> '127.0.0.1',
		'USERNAME'	=> 'root',
		'PASSWORD'	=> '',
		'NAME'		=> 'default',
		'PREFIX'	=> '',
		'TEMPLATE'	=> 'default',
	),
);

define ('LICENSE_KEY','cE7e28fe48d6fe62618747');

// https://developers.google.com/maps/documentation/android/maps-api-signup
define ('GOOGLE_MAPS_KEY','');
define ('GOOGLE_API_KEY','');
define ('BING_APP_ID','');
define ('SECRET_SALT','ABCDEFGHIJ412');
define ('DB_PERSISTENT', false);




